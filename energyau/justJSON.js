/* Simple nodejs server to return json file
*/

var http 	= require('http');
var fs 		= require('fs');
var port 	= "1111" ;
var fcontent = "";
    
http.createServer(function(request, response) {
 
    response.writeHead(200, {
        'Content-Type': 'text/json',
		'Access-Control-Allow-Origin': '*',
		'X-Powered-By':'nodejs'
    });

    
    

    fs.readFile('sample_festival_z.json', function(err, content){
        fcontent  = content ;  
        response.write(fcontent);
        response.end();

    }) ;

       

}).listen(port);

console.log("Listening on port " + port );