import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
/*
 * This java App can be run from command line with the argument of the accompanying server
 * e.g java FestivalConnectionReader http://localhost:1111 
 * The server here is expected to return a JSON complying with:
 *  http://eacodingtest.digital.energyaustralia.com.au/api-docs/; 
 *  server can be started through Node.js  as >: node justJSON.js
 * The resultant output , Bands.html in the project directory, can be opened in a
 * browser.
 author: Pronab Pal
 */
public class FestivalConnectionReader {
	
	static  SortedMap<String, ArrayList<String>> bandEntries  = new TreeMap<>();
	static  SortedMap<String, SortedMap<String, ArrayList<String>>> labelEntries  = new TreeMap<>();
	
    public static String getText(String url) throws Exception {
        URL serversite = new URL(url);
        URLConnection connection = serversite.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                    connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            response.append(inputLine);

        in.close();

        return response.toString();
    }

    public static void main(String[] args) {
        String content = null; JSONArray festivalJSONS = null ;
		try {
			content = FestivalConnectionReader.getText(args[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Expcted Usage: java FestivalConnectionReader http://localhost:1111 ");
			System.exit(999);}
        try {
          festivalJSONS = new JSONArray(content);
          
        }
        catch(JSONException je) {System.out.println("Unable to parse received data :"+ content ) ; 
        System.exit(989);
        }
        
         
        String festivalName= "" ;
        JSONArray bands = null;
        
       for (int fi = 0 ;fi<festivalJSONS.length(); fi ++) {
    	   //extract festival name ; 
    	   try {
    	    festivalName =  (festivalJSONS.getJSONObject(fi)).getString("name");
    	   }
    	   catch(JSONException je) {
    		   System.out.println("no Festival Name found");
    		   System.exit(99);;
    		   
    	   }
    	   //extract band from Bands array
    	   try {
    	    bands =  (festivalJSONS.getJSONObject(fi)).getJSONArray("bands");
    	   }
    	   catch(JSONException je) { 
    		   System.out.println("no Bands found");
    		   System.exit(99);;  
    	   }
    	  for (int bi = 0 ;bi<bands.length(); bi ++) {
    		try {  
    		String bandName = bands.getJSONObject(bi).getString("name") ;
    		String recordLabel = bands.getJSONObject(bi).getString("recordLabel") ;
    		makeLabelEntry(recordLabel,bandName,festivalName)  ; }
    		catch (JSONException je) {
    			 System.out.println(" bands entry: " + bi +"> skipped because data cannot be categorised");
    		}
    	  }
       }
       
       printEntries();
       
    }
        
    
    public static void  makeLabelEntry(String recLabel, String bandName, String festName)
    { 
    	
    	if (labelEntries.containsKey(recLabel))  { 
    		   SortedMap<String, ArrayList<String>> bandEntriesX  = labelEntries.get(recLabel);
    	 	  
    		   if ( bandEntriesX.containsKey(bandName)) { 
    	 		 ArrayList<String> festArrayX =  bandEntriesX.get(bandName);
    	 		festArrayX.add(festName);
	    	 	festArrayX.sort(null);
    	 		  
    	 	  }
    	 	  else {
    	 		   
    	 		 ArrayList<String> festArray =new ArrayList<String>();  
    	    		
       	      
    	    	 	festArray.add(festName);
    	    	 	festArray.sort(null);
    	    		bandEntriesX.put(bandName,festArray );
    	    		labelEntries.put(recLabel,bandEntriesX);
    	 	  }
    		
    		}
    	 
    	else {
    		
    		ArrayList<String> festArray =new ArrayList<String>();  
    		
    	      
    	 	festArray.add(festName);
    	 	festArray.sort(null);
    		bandEntries.put(bandName,festArray );
    		labelEntries.put(recLabel,bandEntries);}
    } 
    static void  printEntries() {
    	Iterator labels = labelEntries.keySet().iterator();
    	StringBuilder htmlResponse = new StringBuilder();
    	while(labels.hasNext()) {
    		String label = (String) labels.next();
    		htmlResponse.append("<h2>" +label + "</h2>");
    		SortedMap<String, ArrayList<String>> bandentries = (SortedMap<String, ArrayList<String>>) labelEntries.get(label);
    		Iterator bands = bandentries.keySet().iterator();
    		while(bands.hasNext()) {
    			String lBand = (String) bands.next();
    			htmlResponse.append("<h3>" + lBand + "</h3>");
    			htmlResponse.append("<ul>");
    			ArrayList<String> fests  = bandentries.get(lBand);
    			for(String fest : fests) {
    				htmlResponse.append("<li>" + fest + "</li>");
    			} 
    			htmlResponse.append("</ul>");
    			}
    		}
    	 
    	try (BufferedWriter bw = new BufferedWriter(new FileWriter("Bands.html"))) {
    		bw.append(htmlResponse);
    		bw.flush();
    		System.out.println("Result Published in Bands.html in project directory");
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    }
}